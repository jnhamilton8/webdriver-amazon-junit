package jnhamilton.selenium.utils;

import io.github.bonigarcia.wdm.WebDriverManager;
import lombok.extern.slf4j.Slf4j;
import org.openqa.selenium.PageLoadStrategy;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.edge.EdgeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.support.events.EventFiringDecorator;
import org.openqa.selenium.support.events.WebDriverListener;

@Slf4j
public class DriverFactory {

    private WebDriver driver;

    public WebDriver driverInit(String browserName) {
        switch (browserName.toLowerCase()) {
            case "chrome":
                createChromeDriver();
                break;
            case "firefox":
                createFireFoxDriver();
                break;
            case "edge":
                createEdgeDriver();
                break;
            default:
                createChromeDriver();
                break;
        }

        WebDriverListener listener = new WebEventListener();

        return new EventFiringDecorator(listener).decorate(driver);
    }

    private void createChromeDriver() {
        WebDriverManager.chromedriver().setup();

        ChromeOptions options = new ChromeOptions();
        options.addArguments("disable-plugins");
        options.addArguments("disable-extensions");
        options.addArguments("test-type");

        driver = new ChromeDriver();
    }

    private void createFireFoxDriver() {
        WebDriverManager.firefoxdriver().setup();

        FirefoxOptions options = new FirefoxOptions();
        options.addArguments("disable-plugins");
        options.addArguments("disable-extensions");

        driver = new FirefoxDriver();
    }

    private void createEdgeDriver() {
        WebDriverManager.edgedriver().setup();

        EdgeOptions edgeOptions = new EdgeOptions();
        edgeOptions.setPageLoadStrategy(PageLoadStrategy.EAGER);

        driver = new EdgeDriver(edgeOptions);
    }
}
