package jnhamilton.selenium.utils;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.*;
import org.junit.jupiter.api.extension.ExtendWith;
import org.openqa.selenium.WebDriver;

@Slf4j
@Getter
@Setter
@NoArgsConstructor(force = true)
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@ExtendWith(TestResultExtension.class)
public class TestBase {

    public WebDriver driver;

    public void initDriver(String browserName) {
        DriverFactory driverFactory = new DriverFactory();
        setDriver(driverFactory.driverInit(browserName));
    }

    @BeforeAll
    public void beforeClass() {
        String browserName = getSystemProperty("browserName");
        initDriver(browserName);
        driver.manage().window().maximize();
    }

    @AfterAll
    public void afterClass() {
        if (getDriver() != null) {
            getDriver().quit();
            setDriver(null);
        }
    }

    public String getSystemProperty(String property) {
        String value = System.getProperty(property);
        if (value == null) {
            Assertions.fail("No value associated with property: " + property + " please check!");
        }

        log.info("Retrieved the value: ({}) for the property: ({})", value, property);
        return value;
    }
}
