package jnhamilton.selenium.utils;

import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.openqa.selenium.*;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Wait;

import java.time.Duration;
import java.util.List;
import java.util.concurrent.TimeUnit;

@Slf4j
@Getter
public class CommonUtils extends TestBase {

    private final static int timeout = 10;
    private final static int poll = 500;
    public Wait<WebDriver> wait;
    public WebDriver driver;

    public CommonUtils(WebDriver driver) {
        this.driver = driver;
    }

    public void navigateToURL(String URL) {
        try {
            driver.navigate().to(URL);
            wait.until(ExpectedConditions.urlToBe(URL));
        } catch (Exception e) {
            log.error("ERROR: URL did not load: " + URL);
        }
    }

    public void click(By selector) {
        WebElement element = getElement(selector);
        waitForElementToBeClickable(selector);
        try {
            element.click();
        } catch (Exception e) {
            throw new WebDriverException("The element " + element.toString() + " is not clickable");
        }
    }

    public void sendKeys(By selector, String value) {
        WebElement element = getElement(selector);
        clearField(element);
        try {
            element.sendKeys(value);
        } catch (Exception e) {
            throw new WebDriverException(String.format("Error in sending [%s] to the following element: [%s]", value, selector.toString()));
        }
    }

    public void clearField(WebElement element) {
        try {
            element.clear();
            waitForElementTextToBeEmpty(element);
        } catch (Exception e) {
            log.error("The element " + element.toString() + " could not be cleared");
        }
    }

    public void waitForElementTextToBeEmpty(WebElement element) {
        String text;
        try {
            text = element.getText();
            int maxRetries = 3;
            int retry = 0;
            while ((text.length() >= 1) || (retry < maxRetries)) {
                retry++;
                waitForXMilliSeconds(500);
                text = element.getText();
            }
        } catch (Exception e) {
            log.error("The element " + element.toString() + " text was not cleared");
        }
    }

    public WebElement getElement(By selector) {
        try {
            return driver.findElement(selector);
        } catch (Exception e) {
            log.error(String.format("The element %s could not be found: ", selector.toString()));
        }
        return null;
    }

    public boolean isElementPresent(By selector, int timeout) {
        wait = returnFluentWait(timeout, poll);
        try {
            return wait.until(ExpectedConditions.visibilityOfElementLocated(selector)).isDisplayed();
        } catch (TimeoutException ex) {
            return false;
        }
    }

    public String getElementText(By selector) {
        waitForElementToBeVisible(selector);
        try {
            return driver.findElement(selector).getText();
        } catch (Exception e) {
            log.error("Element %s does not exist " + selector.toString());
        }
        return null;
    }

    public void waitForElementToBeVisible(By selector) {
        try {
            wait = returnFluentWait(timeout, poll);
            wait.until(ExpectedConditions.visibilityOfElementLocated(selector));
        } catch (Exception e) {
            throw new NoSuchElementException(String.format("The following element did not appear: %s", selector.toString()));
        }
    }

    public void waitForElementToBeClickable(By selector) {
        try {
            wait = returnFluentWait(timeout, poll);
            wait.until(ExpectedConditions.elementToBeClickable(selector));
        } catch (Exception e) {
            throw new WebDriverException("The following element is not clickable: " + selector);
        }
    }

    public void selectItemOnHoverOverMenu(By menu, By menuItem) {
        try {
            WebElement menuElement = getElement(menu);
            Actions builder = new Actions(driver);
            builder.moveToElement(menuElement).build().perform();

            //allow time for the menu contents to appear
            waitForElementToBeClickable(menuItem);
            WebElement menuOption = getElement(menuItem);
            menuOption.click();
        } catch (Exception e) {
            throw new WebDriverException(String.format("The menu: [%s] or the menu item [%s]could not be found or clicked on", menu, menuItem));
        }
    }

    public String getCurrentURL() {
        try {
            return driver.getCurrentUrl();
        } catch (Exception e) {
            throw new WebDriverException("Could not get the current URL");
        }
    }

    public List<WebElement> getElements(By selector) {
        waitForElementToBeVisible(selector);
        try {
            return driver.findElements(selector);
        } catch (Exception e) {
            throw new NoSuchElementException(String.format("The following element did not display: [%s] ", selector.toString()));
        }
    }

    public void waitForXMilliSeconds(long timeOut) throws InterruptedException {
        TimeUnit.MILLISECONDS.sleep(timeOut);
    }

    private Wait<WebDriver> returnFluentWait(int timeout, int poll) {
        return new FluentWait<>(driver)
                .withTimeout(Duration.ofSeconds(timeout))
                .pollingEvery(Duration.ofMillis(poll))
                .ignoring(java.util.NoSuchElementException.class);
    }
}