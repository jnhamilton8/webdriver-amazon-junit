package jnhamilton.selenium.utils;

import io.qameta.allure.Allure;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.events.WebDriverListener;

public class WebEventListener implements WebDriverListener {

    @Override
    public void beforeTo(WebDriver.Navigation navigation, String url) {
        Allure.step("Navigating to: " + url);
    }

    @Override
    public void afterTo(WebDriver.Navigation navigation, String url) {
        Allure.step("Navigated to: " + url);
    }

    @Override
    public void beforeFindElement(WebDriver driver, By by) {
        Allure.step("Attempting to find element by : " + by.toString());
    }

    @Override
    public void afterFindElement(WebDriver driver, By by, WebElement result) {
        Allure.step("Found element by: " + by.toString());
    }

    @Override
    public void beforeClick(WebElement element) {
        Allure.step("Attempting to click on: " + element.toString());
    }

    @Override
    public void afterClick(WebElement element) {
        Allure.step("Clicked on: " + element.toString());
    }

    @Override
    public void beforeGetText(WebElement element) {
        Allure.step("Attempting to get text from element: " + element.toString());
    }

    @Override
    public void afterGetText(WebElement element, String text) {
        Allure.step("Got text: " + text + " from element: " + element.toString());
    }
}
