package jnhamilton.selenium.testcases;

import jnhamilton.selenium.testdata.LoadProduct;
import jnhamilton.selenium.utils.PageObjectFactory;
import jnhamilton.selenium.utils.TestBase;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;
import static org.hamcrest.core.Is.is;

public class BasketTest extends TestBase {

    private PageObjectFactory pageObjectFactory;

    @BeforeAll
    public void setUp() {
        pageObjectFactory = new PageObjectFactory(getDriver());
        pageObjectFactory.setupPageObjects(List.of("homePage", "productPage", "topBanner", "shoppingBasketPage"));
    }

    @BeforeEach
    public void beforeMethod() throws InterruptedException {
        pageObjectFactory.homePage().navigateToHomePage();
        pageObjectFactory.shoppingBasketPage().removeAllItemsFromBasketIfPresent();
    }


    @Test
    public void canAddItemsToBasket() {
        pageObjectFactory.productPage().addItemToBasket("book1");
        pageObjectFactory.productPage().addItemToBasket("book2");

        assertThat("The number of items in the basket did not match that expected", pageObjectFactory.topBanner().getNumberOfItemsListedInShoppingCartIcon(), is("2"));

        pageObjectFactory.topBanner().selectShoppingCartIcon();
        ArrayList<String> basketItems = pageObjectFactory.shoppingBasketPage().getHeadersOfAllItemsInBasket();

        assertThat("The items in the Basket did not match those expected", basketItems, hasItems(new LoadProduct().getBookTitle("book1"),
                new LoadProduct().getBookTitle("book2")));
    }

    @Test
    public void canDeleteItemsFromBasket() throws InterruptedException {
        pageObjectFactory.productPage().addItemToBasket("book1");
        pageObjectFactory.productPage().addItemToBasket("book2");

        pageObjectFactory.shoppingBasketPage().removeAllItemsFromBasketIfPresent();
        pageObjectFactory.topBanner().selectShoppingCartIcon();

        assertThat("The basket was not empty as expected", pageObjectFactory.topBanner().getNumberOfItemsListedInShoppingCartIcon(), is("0"));
        assertThat("The shopping basket message wasn't as expected", pageObjectFactory.shoppingBasketPage().getShoppingCartMessage(), is("Your Amazon basket is empty"));
    }
}