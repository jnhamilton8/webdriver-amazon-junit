package jnhamilton.selenium.testcases.frameworktests;

import jnhamilton.selenium.utils.TestBase;
import jnhamilton.selenium.utils.Urls;
import org.junit.jupiter.api.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsString;

public class DriverManagerTest extends TestBase {

    @Test
    public void canWeFireUpTheDriver() {
        driver.get(Urls.BASE_URL.getUrl());

        assertThat(driver.getTitle(), containsString("Amazon.co.uk"));
    }
}