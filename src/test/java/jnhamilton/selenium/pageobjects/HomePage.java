package jnhamilton.selenium.pageobjects;

import jnhamilton.selenium.utils.CommonUtils;
import jnhamilton.selenium.utils.Urls;
import lombok.extern.slf4j.Slf4j;
import org.openqa.selenium.By;

@Slf4j
public class HomePage {

    private final static By ACCEPT_COOKIES_BUTTON = By.xpath("//div[contains(@class,'sp-cc-buttons')]//span[contains(@class,'a-button a-button-primary')]");

    private final CommonUtils commonUtils;

    public HomePage(CommonUtils commonUtils) {
        this.commonUtils = commonUtils;
    }

    public void navigateToHomePage() {
        commonUtils.getDriver().get(Urls.BASE_URL.getUrl());
        acceptCookies();
    }

    private void acceptCookies() {
        if (commonUtils.isElementPresent(ACCEPT_COOKIES_BUTTON, 2)) {
            commonUtils.click(ACCEPT_COOKIES_BUTTON);
        }
        log.info("Accepted the cookies for the duration of the session");
    }
}