# README #

### What is this repository for? ###

A personal Webdriver project, setting up a UI testing framework from scratch and automating tests using Amazon as an example.
This framework is based around Java and JUnit5, and includes Selenium Webdriver, WebDriver Manager, Maven, Lombok, Allure Reports and others :)

The driver is instantiated in one place, TestBase, which receives as a system property the browser to use. 
Each test class then extends TestBase, receiving a copy of the driver without a need to pass it around like a hot potato. 
Test classes request the page objects they need using the PageObjectFactory, which receives a list of page objects and instantiates those needed
and stores them in a map which the test can access as required.
All page objects use CommonUtils, which abstracts common Webdriver functions.

Please note as I do obviously not have access to Amazon backend, a few tasks where I would, for example, delete or create test data via backend jobs
have been done via the GUI. 

### How do I run the tests? ###

The tests can be run via maven on the command line using `mvn test`, or just to run one test `mvn -Dtest=<testname> test`   
This will pick up the default browserName system property as set in the pom.xml, which is chrome.

The browser to use however can be changed at runtime: `mvn test -DbrowserName=<browser>`

You can run the test locally using Junit as usual, passing in the system property for the browser.  If none is supplied
then DriverFactory will use Chrome by default. 

The framework supports Chrome, Firefox, Edge and IE. 

### Reporting ###
Allure Reports is used to show test results. Allure is set up in the pom.xml file, however, 
to run Allure locally you need to:

- install Scoop (https://scoop.sh/)
- follow the instructions here: https://docs.qameta.io/allure/

When installed, after tests have run, you can run: `allure serve allure-results`

Screenshots on failure will be captured and be visible in the report.  This is done via the TestResultExtension class.
Further, Allure will report on webdriver commands executed during the test, which is done via WebEventListener and using an EventFiringWebdriver.
This can help with debugging and reduce the need for log statements in the tests and page objects. 